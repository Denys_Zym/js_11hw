// Завдання
// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const lables = document.querySelectorAll(".input-wrapper");

lables.forEach((i) => i.addEventListener("click", showPassword));

function showPassword(e) {
  if (e.target.nodeName === "I") {
    console.log(e.target);
    const input = e.target.parentElement.querySelector("input");
    console.log(input);
    e.target.classList.toggle("fa-eye-slash");
    e.target.classList.toggle("fa-eye");
    input.type = input.type === "password" ? "text" : "password";
  }
}

const btn = document.querySelector(".btn");
const inputs = document.querySelectorAll("input");
const err = document.querySelector(".error");

btn.addEventListener("click", chekPassword);

function chekPassword(e) {
  if (e.target.nodeName === "BUTTON") {
    e.preventDefault();
    if (!inputs[0].value) {
      err.innerText = "Нужно ввести значения";
      err.style.color = "red";
    } else if (inputs[0].value === inputs[1].value) {
      err.innerText = "";
      alert("You are welcome");
    } else {
      err.innerText = "Нужно ввести одинаковые значения";
      err.style.color = "red";
    }
  }
}

//-----------------------------------------------------------

// const icon = document.querySelector(".icon-password");
// console.log(icon);
// const input = document.querySelector("input");
// console.log(input);

// input.addEventListener("click", showPassord);

// function showPassord() {
//   if (input.getAttribute("type") == "password") {
//     input.removeAttribute("type");
//     input.setAttribute("type", "text");
//     icon.className = "far fa-eye-slash";
//   } else {
//     input.removeAttribute("type");
//     input.setAttribute("type", "password");
//     icon.className = "far fa-eye";
//   }
// }
// showPassord();
